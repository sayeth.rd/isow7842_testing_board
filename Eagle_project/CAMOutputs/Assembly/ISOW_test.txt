Partlist exported from /Users/Sayeth/Documents/EAGLE/projects/ISOW_TEST/ISOW_test.sch at 29/10/19 12:06

Qty Value Device                    Package  Parts                                                                                     Description               MANUFACTURER      PARTNUMBER   PROD_ID   REFDES TYPE VALUE
16        PINHD-1X1                 1X01     JP5, JP6, JP7, JP8, JP9, JP10, JP11, JP12, JP13, JP14, JP15, JP16, JP17, JP18, JP19, JP20 PIN HEADER                                                                          
2         PINHD-1X3                 1X03     JP3, JP4                                                                                  PIN HEADER                                                                          
2         PINHD-1X7                 1X07     JP1, JP2                                                                                  PIN HEADER                                                                          
2   0.1uF 1.0UF-0805-25V-(+80/-20%) 0805     C3, C6                                                                                    1µF ceramic capacitors                                   CAP-11625             1.0uF
2   1.0uF 1.0UF-0805-25V-(+80/-20%) 0805     C2, C5                                                                                    1µF ceramic capacitors                                   CAP-11625             1.0uF
2   10uF  10UF-0805-10V-10%         0805     C1, C4                                                                                    10.0µF ceramic capacitors                                CAP-11330             10uF 
1   Value ISOW7842DWER              DWE0016A U1                                                                                                                  Texas Instruments ISOW7842DWER           RefDes TYPE Value
